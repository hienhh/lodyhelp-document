# Be A Host Website Documentation

### Giới thiệu
>>>
Be A Host là một module khác của hệ thống LodyHelp nhằm phục vụ mục đích cho phép người dùng đăng chỗ nghỉ lên hệ thống của LodyHelp.
>>>

### Thông tin hệ thống
>>>

**Front End:** VueJs, NuxtJs, Vuetify, Material Template

**Back End:** WordPress Rest Full API

**Database:** MySQL

>>>

### Cấu trúc thư mục

![alt text](images/directory-structure.png "Cấu trúc thư mục của dự án")
###### Giải thích chi tiết thư mục bên dưới
- **assets** Thư mục này để lưu những file cho giao diện web như font, stylesheet, icon, vector
- **components** Thư mục này để lưu các components của module có thể dùng lại ở trang khác nhau
- **configs** Thư mục này chứa những file cấu hình cho website
- **locales** Thư mục này chứa những file dịch cho những label trên website
- **middleware** Thư mục này chứa những file xử lý middleware của website
- **node_modules** Thư mục node packages
- **page** Thư mục chứa các trang của website
- **plugins** Thư mục chứa những plugin khác cài thêm hoặc tự viết viết thêm để xử lý một số chức năng đặc biệt
- **static** Thư mục chứa những static file cho hệ website
- **store** Thư mục chứa những thông của vuex ở đây
- **test** Thư mục chứa file về unit test
- **untils** Thư mục chứa những những file liên quan đến xử lý API hoặc file helper
- **app.json** File này là file lưu cấu hình hệ thống ví dụ như API url, hay là preview url
- **nuxt.config.js** File này để cấu hình cho dự án sử dụng nuxtjs. Nếu muốn xem thêm thông tin của nuxtjs thì xem ở link **[này](https://nuxtjs.org/guide/configuration)**
- **package.json** Đây là file cấu hình cho dự án sử dụng các package của node js cũng như các scripts để chạy dự án.
- **server.json** Đây là file để cấu hình chạy trên server sau này deploy dự án sử dụng **[PM2](http://pm2.keymetrics.io/)** để quản lý các process


#### Những màn hình chính của hệ thống Be A Host

1. [Đăng một chổ nghỉ mới](submit-property.md)
2. [Những chổ nghỉ của tôi](my-properties.md)
3. [Xác nhận quyền sở hữu](confirm-ownership.md)
4. [Quản lý đặt phòng chổ nghỉ](management-booking.md)
5. [Tiếp tục đăng chổ nghỉ chưa hoàn thành](continue-submit-property.md)
6. [Cập nhật chổ nghỉ đã đăng thành công](update-property.md)
7. [Quản lý chổ nghỉ của tất cả người dùng đã đăng](dashboard-property.md) **chỉ dành cho admin của website**
8. [Cập nhật thông tin cá nhân](my-profile.md)


>>>
Đang cập nhật tiếp ...
>>>