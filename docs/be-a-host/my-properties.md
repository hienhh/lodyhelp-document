## Những chổ nghỉ của tôi
>>>
Đây là trang để hiển thị danh sách những chổ nghỉ đã đăng từ trước tới giờ.
>>>

Đường dẫn của màn hình này sẽ có nhiều cách để có thể chuyển đến màn hình này.
1. Khi đăng bài đăng thành công thì nó sẽ tự chuyển sang màn hình.
2. Click nút **Những chổ nghỉ của tôi** trên menu
3. Hoặc chạy đường dẫn bên dưới

```
https://host.lodyhelp.com/vi/property
```


Nội dung màn hình này được lưu ở file dưới
```
pages/_lang/property/index.vue
```