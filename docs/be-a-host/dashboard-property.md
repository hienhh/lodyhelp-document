## Quản lý danh sách chổ nghỉ của host(dành riêng cho admin).

>>>
Đây là trang quản lý dành riêng cho admin để cho phép hoặc từ chối bài đăng của host lên hệ thống LodyHelp.
Đường dẫn tới trang này như sau
```
/vi/dashboard
```
File lập trình cho trang này là
```
pages/_lang/dashboard/index.vue
```
>>>

Chúng ta có xài một component để lấy danh sách chổ nghỉ cho trang dashboard này như sau
```html
<Dashboard :url="url" @changeSelected="changeSelected" :reload="reload" />
```
Trong đó ta có 2 props là `url` và `reload` và một custom event là `changeSelected`

`url` là địa chỉ API để lấy danh sách chổ nghỉ
`reload` là có cần tải lại data hay không
`changeSelected` là event sẽ được gọi khi người dùng click vào checkbox trong datatable

Có thể xem thêm thông tin chi tiết component Dashboard ở thư mục
```
components/dashboard/Dashboard.vue
```

Mô tả về những phương thức(methods) của  page như bên dưới

```javascript
methods: {
  loadStatusCount(){
    // Phương thức này dùng để lấy số lượng chổ nghỉ theo trạng thái
  },
  changeUrl(status){
    // Phương thức này để thay đổi lại url của datatable, ví dụ tìm kiếm theo trạng thái
  },
  search(){
    // Tìm kiếm chổ nghỉ theo từ khóa đã nhập
  },
  changeSelected(data){
    // Khi thay đổi lại những items đã selected. Cái này dùng để thay đổi trạng thái hàng loạt
  },
  submitBulkAction(){
    // Xử lý chổ nghỉ theo hành động của admin
  },
  translatedItems(){
    // Lấy tất cả chổ nghỉ đã dịch
  }
}
```

Nếu ở màn hình này chúng ta click vào icon có hình cây bút thì nó sẽ chuyển sang màn hình cập nhật và xác nhận bài đăng. Hầu như tất cả chức năng khá giống như cập nhật chổ nghỉ của host. Nhưng nó khác chổ là admin cần phải dịch lại thông tin cho bài đăng.