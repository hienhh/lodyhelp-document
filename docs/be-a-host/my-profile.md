## Trang cập nhật thông tin của tôi(update profile)

Đường dẫn của trang này ở url như sau
```
/vi/update-profile
```
File lập trình cho trang này như sau
```
pages/_lang/update-profile.vue
```

Ở màn hình này chúng ta có những phương thức chính như sau

```javascript
methods: {
  updateProfile: function(){
    // Gọi API để cập nhật thông tin tài khoản đang đăng nhập
  },
  processLogout: function(){
    // Đăng xuất
  },
  changeLanguage: function(locale){
    // Thay đổi ngôn ngữ hiển thị
  },
  goMyProfile(){
    // Chuyển sang trang thông tin cá nhân
  },
  upload_user_avt(e){
    // Upload user avatar
  }
}
```