# Tài liệu dự án LodyHelp
#### Dự án LodyHelp web có 2 phần chính là:
>>>
[Lody Help](https://lodyhelp.com) Đây là website chính của hệ thống. Document về dự án Lody Help ở [đây](lody-help/README.md)
>>>

>>>
[Be A Host](https://host.lodyhelp.com) Đây là một phần khác của hệ thống để cung cấp chức năng cho người dùng có thể đăng chổ nghỉ lên hệ thống của LodyHelp. Document về module ***Be A Host*** ở [đây](be-a-host/README.md)
>>>
